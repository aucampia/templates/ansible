#!/usr/bin/env bash

script_name="${0}"
script_dirname="$( dirname "${0}" )"
script_basename="$( basename "${0}" )"

venv="${script_dirname}/var/venv"

[ -e "${venv}/.ready" ] || {
	[ -e "${venv}/bin/pip" ] || python3 -m virtualenv --never-download --no-site-packages "${venv}"
	"${venv}/bin/pip" install 'ansible'
	touch "${venv}/.ready"
}

source var/venv/bin/activate
"${@}"
