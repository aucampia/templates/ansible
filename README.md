# ...

## template content

```bash
pip3 install --user --upgrade ansible

./venv.sh ansible-config dump

[ -e ~/.ansible/aucampia-template.passwd ] || pwgen 16 1 > ~/.ansible/aucampia-template.passwd

./venv.sh ansible-vault -v create inventory/group_vars/all/vars.vault.yaml
./venv.sh ansible-vault -v edit inventory/group_vars/all/vars.vault.yaml
./venv.sh ansible-vault -v view inventory/group_vars/all/vars.vault.yaml

./venv.sh ansible-inventory --list
```

```bash
diff -x.git -xvar -r ./ /... | grep ^Only
diff -x.git -xvar -r ./ /... | grep ^diff
```
